sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
sudo apt update

sudo apt-fast install mono-devel
sudo apt-fast install mono-complete
sudo apt-fast install mono-dbg
sudo apt-fast install referenceassemblies-pcl
sudo apt-fast install ca-certificates-mono
sudo apt-fast install mono-xsp4
